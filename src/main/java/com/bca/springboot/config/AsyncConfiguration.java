package com.bca.springboot.config;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

@Slf4j
@Configuration
@EnableAsync
public class AsyncConfiguration {

	@Bean(name = "taskExecutor")
	public Executor taskExecutor(@Value("${sample.async.core-pool-size:5}") int corePoolSize,
								 @Value("${sample.async.max-pool-size:999}") int maxPoolSize,
								 @Value("${sample.async.queue-capacity:999}") int queueCapacity) {
		log.debug("Creating Async Task Executor");
		log.debug("corePoolSize : " + corePoolSize);
		log.debug("maxPoolSize : " + maxPoolSize);
		log.debug("queueCapacity : " + queueCapacity);
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(corePoolSize);
		executor.setMaxPoolSize(maxPoolSize);
		executor.setQueueCapacity(queueCapacity);
		executor.setThreadNamePrefix("AsynchThread-");
		executor.initialize();
		return executor;
	}

}