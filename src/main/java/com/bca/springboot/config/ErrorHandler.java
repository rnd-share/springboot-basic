package com.bca.springboot.config;

import com.bca.springboot.employee.dto.ResponseSchema;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.server.MethodNotAllowedException;
import org.springframework.web.servlet.NoHandlerFoundException;

@ControllerAdvice
@Slf4j
public class ErrorHandler {

	@ExceptionHandler({MethodNotAllowedException.class, HttpRequestMethodNotSupportedException.class})
	public ResponseEntity<ResponseSchema> errorMethodNotAllowed(Exception e) {
		log.error("Terjadi kesalahan | Method Not Allowed : "+e.getMessage(), e);
		return ResponseEntity.badRequest()
				.body(ResponseSchema.<String>builder()
						.errorSchema(ResponseSchema.error())
						.outputSchema("Method Not Allowed")
						.build());
	}

	@ExceptionHandler(Exception.class)
	public ResponseEntity<ResponseSchema> errorHandlerGlobal(Exception e) {
		log.error("Terjadi kesalahan | Global Exception : "+e.getMessage(), e);
		return ResponseEntity.badRequest()
				.body(ResponseSchema.<String>builder()
						.errorSchema(ResponseSchema.error())
						.outputSchema("Terjadi kesalahan")
						.build());
	}
}
