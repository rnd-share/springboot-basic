package com.bca.springboot.employee.controller;

import com.bca.springboot.employee.dto.ResponseSchema;
import com.bca.springboot.employee.entity.DivisiEntity;
import com.bca.springboot.employee.service.DivisiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("divisi")
public class DivisiController {
	@Autowired
	private DivisiService divisiService;

	@GetMapping
	public ResponseEntity<ResponseSchema<List<DivisiEntity>>> getDivisi() {
		return ResponseEntity.ok()
				.body(ResponseSchema.<List<DivisiEntity>>builder()
						.errorSchema(ResponseSchema.success())
						.outputSchema(divisiService.getAll())
						.build()
				);
	}

	@GetMapping("{id}")
	public ResponseEntity<ResponseSchema<DivisiEntity>> getDivisiUsePathVariable(
			@PathVariable("id") int divisiId
	) {
		return ResponseEntity.ok()
				.body(ResponseSchema.<DivisiEntity>builder()
						.errorSchema(ResponseSchema.success())
						.outputSchema(divisiService.getById(divisiId))
						.build()
				);
	}

	@GetMapping("search")
	public ResponseEntity<ResponseSchema<DivisiEntity>> getDivisiUseRequestParam(@RequestParam("id") int divisiId) {
		return ResponseEntity.ok()
				.body(ResponseSchema.<DivisiEntity>builder()
						.errorSchema(ResponseSchema.success())
						.outputSchema(divisiService.getById(divisiId))
						.build()
				);
	}

	@PostMapping
	public ResponseEntity<ResponseSchema<DivisiEntity>> add(@RequestBody DivisiEntity divisiEntity) {
		return ResponseEntity
				.status(HttpStatus.CREATED)
				.body(ResponseSchema.<DivisiEntity>builder()
						.errorSchema(ResponseSchema.success())
						.outputSchema(divisiService.saveData(divisiEntity))
						.build()
				);
	}

	@PutMapping
	public ResponseEntity<ResponseSchema<DivisiEntity>> update(@RequestBody DivisiEntity divisiEntity) {
		return ResponseEntity
				.status(HttpStatus.OK)
				.body(ResponseSchema.<DivisiEntity>builder()
						.errorSchema(ResponseSchema.success())
						.outputSchema(divisiService.saveData(divisiEntity))
						.build()
				);
	}


	@DeleteMapping("{id}")
	public ResponseEntity<ResponseSchema<String>> delete(@PathVariable("id") int divisiId) {
		divisiService.deleteData(divisiId);
		return ResponseEntity
				.status(HttpStatus.OK)
				.body(ResponseSchema.<String>builder()
						.errorSchema(ResponseSchema.success())
						.outputSchema("success")
						.build()
				);
	}

}
