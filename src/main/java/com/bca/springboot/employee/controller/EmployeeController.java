package com.bca.springboot.employee.controller;

import com.bca.springboot.employee.dto.EmployeeDTO;
import com.bca.springboot.employee.dto.ResponseSchema;
import com.bca.springboot.employee.entity.EmployeeEntity;
import com.bca.springboot.employee.service.EmployeeService;
import jakarta.websocket.server.PathParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("employee")
@Slf4j
public class EmployeeController {

	@Autowired
	private EmployeeService service;

	@GetMapping
	public ResponseEntity<ResponseSchema<List<EmployeeDTO>>> getEmployees() {
		return ResponseEntity.ok()
				.body(ResponseSchema.<List<EmployeeDTO>>builder()
						.errorSchema(ResponseSchema.success())
						.outputSchema(service.getEmployees())
						.build()
				);
	}

	@PostMapping
	public ResponseEntity<ResponseSchema<EmployeeEntity>> add(@RequestBody EmployeeDTO request) {
		return ResponseEntity
				.status(HttpStatus.CREATED)
				.body(ResponseSchema.<EmployeeEntity>builder()
						.errorSchema(ResponseSchema.success())
						.outputSchema(service.save(request))
						.build()
				);
	}

	@PutMapping
	public ResponseEntity<ResponseSchema<String>> update(@RequestBody EmployeeDTO request) {
		service.save(request);
		return ResponseEntity.ok(ResponseSchema.<String>builder()
				.errorSchema(ResponseSchema.success())
				.outputSchema("success")
				.build()
		);
	}

	@DeleteMapping("{nip}")
	public ResponseEntity<ResponseSchema<String>> delete(@PathVariable("nip") String nip) {
		service.delete(nip);
		return ResponseEntity.ok(ResponseSchema.<String>builder()
				.errorSchema(ResponseSchema.success())
				.outputSchema("success")
				.build()
		);
	}

}
