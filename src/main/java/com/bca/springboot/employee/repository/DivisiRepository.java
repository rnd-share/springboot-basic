package com.bca.springboot.employee.repository;

import com.bca.springboot.employee.entity.DivisiEntity;
import com.bca.springboot.employee.entity.EmployeeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DivisiRepository extends JpaRepository<DivisiEntity, Integer> {

	void deleteByDivisiID(Integer id);

}
