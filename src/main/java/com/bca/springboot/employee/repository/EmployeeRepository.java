package com.bca.springboot.employee.repository;

import com.bca.springboot.employee.entity.EmployeeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends JpaRepository<EmployeeEntity, String> {

	@Query("SELECT e FROM EmployeeEntity e")
	List<EmployeeEntity> selectDataWithQuery();

	@Query(value = "SELECT * FROM employee", nativeQuery = true)
	List<EmployeeEntity> selectDataWithQueryNative();

	void deleteByNip(String nip);


}
