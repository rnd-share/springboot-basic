package com.bca.springboot.employee.service;

import com.bca.springboot.employee.dto.EmployeeDTO;
import com.bca.springboot.employee.entity.EmployeeEntity;
import com.bca.springboot.employee.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository repository;

    public List<EmployeeDTO> getEmployees() {
        return repository.selectDataWithQueryNative().stream()
                .map( e -> {
                    return EmployeeDTO.builder()
                            .nip(e.getNip())
                            .name(e.getName())
                            .build();
                })
                .collect(Collectors.toList());
    }

    public EmployeeEntity save(EmployeeDTO request) {
        return repository.save(EmployeeEntity.builder()
                        .nip(request.getNip())
                        .name(request.getName())
                .build());
    }

    public void delete(String nip) {
        repository.deleteById(nip);
    }

}
