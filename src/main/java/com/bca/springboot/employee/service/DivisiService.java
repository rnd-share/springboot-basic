package com.bca.springboot.employee.service;

import com.bca.springboot.employee.entity.DivisiEntity;
import com.bca.springboot.employee.repository.DivisiRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DivisiService {

	@Autowired
	private DivisiRepository divisiRepository;

	//	Untuk select all
	public List<DivisiEntity> getAll() {
		return divisiRepository.findAll();
	}

	//	Untuk get by id
	public DivisiEntity getById(int id) {
		return divisiRepository.findById(id).get();
	}

	//	Untuk simpan dan update data
	public DivisiEntity saveData(DivisiEntity divisiEntity) {
		return divisiRepository.save(divisiEntity);
	}

	//	Untuk delete data
	public void deleteData(int divisiId) {
		divisiRepository.deleteByDivisiID(divisiId);
	}

}
