package com.bca.springboot.employee.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseSchema<T> {

    private ErrorSchema errorSchema;
    private T outputSchema;

    @Builder
    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    static public class ErrorSchema {

        private String errorCode;
        private ErrorMessage errorMessage;

        @Builder
        @Data
        @AllArgsConstructor
        @NoArgsConstructor
        static public class ErrorMessage {

            private String messageId;
            private String messageEn;

        }

    }

    static public ErrorSchema success() {
        return ResponseSchema.ErrorSchema.builder()
                .errorCode("EMP-00-000")
                .errorMessage(ResponseSchema.ErrorSchema.ErrorMessage.builder()
                        .messageId("Berhasil")
                        .messageEn("Success")
                        .build())
                .build();
    }


    static public ErrorSchema error() {
        return ResponseSchema.ErrorSchema.builder()
                .errorCode("NIS-99-999")
                .errorMessage(ResponseSchema.ErrorSchema.ErrorMessage.builder()
                        .messageId("Gagal")
                        .messageEn("Failed")
                        .build())
                .build();
    }

}
