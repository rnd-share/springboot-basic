## alpine linux
# base image dari internal registry
# FROM registry.dti.co.id/public/openjdk:17-alpine

# base image dari internet (docker.hub)
FROM openjdk:17-alpine


## set the working directory
WORKDIR /app

## copy the spring jar
COPY target/springboot-basic.jar  .

ENTRYPOINT ["java", "-jar", "springboot-basic.jar"]

## expose port
EXPOSE 8080